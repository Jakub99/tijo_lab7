package pl.edu.pwsztar.domain.interfaces;

@FunctionalInterface
public interface Converter<T, F> {
    T convert(F from);
}
